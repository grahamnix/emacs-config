;; Before using this, you need to have use-package installed.
;; M-x package-install use-package

;; Some package setup
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/")
	     '("melpa-stable" . "https://melpa.org/packages/"))
(package-initialize)
(eval-when-compile
  (require 'use-package))

;; evil
(use-package evil
  :ensure t
  :init
  (define-key key-translation-map (kbd "ESC") (kbd "C-g"))
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :ensure t
  :after evil
  :init
  (evil-collection-init))

(use-package evil-commentary
  :ensure t
  :after evil
  :init
  (evil-commentary-mode))

(use-package evil-magit
  :ensure t
  :after magit)

;; Visual tweaks
(use-package zenburn-theme
  :ensure t
  :init
  (load-theme 'zenburn t)
  (menu-bar-mode -1)
  (toggle-scroll-bar -1)
  (tool-bar-mode -1))

;; Helm + vimlike Helm
(use-package helm
  :ensure t
  :bind (("M-x" . helm-M-x)
	 ("<menu>" . helm-M-x))
  :config
  (define-key helm-map (kbd "C-j") 'helm-next-line)
  (define-key helm-map (kbd "C-k") 'helm-previous-line)
  (define-key helm-map (kbd "C-h") 'helm-next-source)
  (define-key helm-map (kbd "C-l") (kbd "RET"))
  (with-eval-after-load 'helm-files
    (dolist (keymap (list helm-find-files-map helm-read-file-map))
      (define-key keymap (kbd "C-l") 'helm-execute-persistent-action)
      (define-key keymap (kbd "C-h") 'helm-find-files-up-one-level))))

(use-package which-key
  :ensure t
  :init
  (which-key-mode))

;; Leader keybinds
(use-package general
  :ensure t
  :init
  (defconst my-leader "SPC")
  (general-create-definer space-leader-def
    :prefix "SPC")
  (space-leader-def
    :keymaps 'normal
    "e e" 'cider-eval-defun-at-point
    "e d" 'cider-debug-defun-at-point
    "e b" 'cider-eval-buffer
    "e e" 'cider-eval-buffer
    "b b" 'helm-buffers-list
    "f f" 'helm-find-files
    "g s" 'magit-status
    "RET" 'helm-M-x))

;; No configuration packages
(use-package projectile
  :ensure t)
(use-package helm-projectile
  :ensure t)
(use-package helm-ag
  :ensure t)
(use-package magit
  :ensure t)
(use-package guix
  :ensure t)
(use-package nix-mode
  :ensure t)
(use-package cider
  :ensure t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (cider helm-ag helm-projectile projectile nix-mode guix emacs-guix rebase-mode evil-collection evil-commentary general use-package which-key helm evil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
